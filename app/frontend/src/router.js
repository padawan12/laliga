import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Club_Details from './views/Club_Details.vue'
import Club_Players from './views/Club_Players.vue'
import Player_Details from './views/Player_Details.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/club/:club',
      name: 'club_details',
      component: Club_Details
    },
    {
      path: '/club/:club/players',
      name: 'club_players',
      component: Club_Players
    },
      {
      path: '/club/:club/players/:player',
      name: 'player_details',
      component: Player_Details
    }
  ]
})
