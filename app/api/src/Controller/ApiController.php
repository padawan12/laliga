<?php
namespace Api\Controller;

use Api\Entity\Club;
use Api\Entity\Player;
use Api\Form\PlayerType;
use Api\Service\ClubService;
use Api\Service\PlayerService;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ApiController extends FOSRestController
{
    /**
     * Get clubs list
     *
     * @Annotations\Get("/clubs")
     * @Annotations\View(serializerGroups={"list"})
     *
     */
    public function getClubsAction(Request $request, ClubService $clubService)
    {
        $division = $request->get('d', 1);

        return $clubService->findBy(['division' => Club::DIVISIONS[$division]], ['name' => 'ASC']);
    }

    /**
     * Get a club
     *
     * @Annotations\Get("/clubs/{club}")
     * @Annotations\View(serializerGroups={"list", "details"})
     */
    public function getClubAction(Club $club)
    {
        return $this->view($club)->setContext((new Context())->addGroup('list'));
    }

    /**
     * Get all club player
     *
     * @Annotations\Get("/clubs/{club}/players")
     * @Annotations\View(serializerGroups={"list"})
     */
    public function getClubPlayersAction(Club $club)
    {
        return $club->getPlayers();
    }

    /**
     * Get a club player
     *
     * @Annotations\Get("/clubs/{club}/players/{player}")
     * @Annotations\View(serializerGroups={"list", "details"})
     */
    public function getClubPlayerAction(Club $club, Player $player)
    {
        return $player;
    }

    /**
     * Create a club player
     *
     * @Annotations\Post("/clubs/{club}/players")
     */
    public function postClubPlayerAction(Club $club, Request $request, PlayerService $playerService)
    {
        $player = new Player();

        $data = json_decode($request->getContent(), true);

        $form = $this->createForm(PlayerType::class, $player);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $player->setClub($club);
            $playerService->persist($player);

            return new Response(null, 201);
        }

        return new Response(null, 422);
    }
}