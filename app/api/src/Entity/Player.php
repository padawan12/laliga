<?php
namespace Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Player")
 */
class Player
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", name="player_id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="player_name")
     * @JMS\Groups({"list"})
     */
    protected $name;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="player_dorsal")
     * @JMS\Groups({"list"})
     */
    protected $dorsal;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="player_position")
     * @JMS\Groups({"details"})
     */
    protected $position;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="player_nationality")
     * @JMS\Groups({"details"})
     */
    protected $nationality;

    /**
     * @var Club
     *
     * @ORM\ManyToOne(targetEntity="Club", inversedBy="players")
     * @ORM\JoinColumn(name="club_id", referencedColumnName="club_id")
     */
    protected $club;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return integer
     */
    public function getDorsal()
    {
        return $this->dorsal;
    }

    /**
     * @param integer $dorsal
     *
     * @return self
     */
    public function setDorsal($dorsal)
    {
        $this->dorsal = $dorsal;

        return $this;
    }

    /**
     * @return Club
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param Club $club
     *
     * @return self
     */
    public function setClub(Club $club)
    {
        $this->club = $club;

        return $this;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     *
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     *
     * @return self
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }
}