<?php
namespace Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Club")
 */
class Club
{
    const DIVISIONS = [
        1 => 'Liga Santander',
        2 => 'Liga 1|2|3'
    ];

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", name="club_id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="club_name")
     * @JMS\Groups({"list"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="club_city")
     * @JMS\Groups({"list"})
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="club_stadium")
     * @JMS\Groups({"details"})
     */
    protected $stadium;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="club_division")
     * @JMS\Groups({"details"})
     */
    protected $division;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="club_coach")
     * @JMS\Groups({"details"})
     */
    protected $coach;

    /**
     * @var Player[]
     *
     * @ORM\OneToMany(targetEntity="Player", mappedBy="club")
     */
    protected $players;

    public function __construct() {
        $this->players = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Player
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @param $players
     *
     * @return self
     */
    public function setPlayers($players)
    {
        $this->players = $players;

        return $this;
    }

    /**
     * @param Player $player
     *
     * @return self
     */
    public function addPlayer(Player $player)
    {
        if(! $this->players->contains($player)) {

            $this->players->add($player);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getStadium()
    {
        return $this->stadium;
    }

    /**
     * @param string $stadium
     *
     * @return self
     */
    public function setStadium($stadium)
    {
        $this->stadium = $stadium;

        return $this;
    }

    /**
     * @return string
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * @param string $division
     *
     * @return self
     */
    public function setDivision($division)
    {
        $this->division = $division;

        return $this;
    }

    /**
     * @return string
     */
    public function getCoach()
    {
        return $this->coach;
    }

    /**
     * @param string $coach
     *
     * @return self
     */
    public function setCoach($coach)
    {
        $this->coach = $coach;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }
}