<?php
namespace Api\Service;

use Doctrine\Common\Persistence\ManagerRegistry as Doctrine;

class ClubService
{
    protected $doctrine;

    protected $repository;

    public function __construct(Doctrine $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->repository = $this->doctrine->getRepository('Api\\Entity\\Club');
    }

    public function findAll($order = ['name' =>  'ASC'])
    {
        return $this->repository->findBy([], $order);
    }

    public function findBy($criteria, $order = ['name' =>  'ASC'])
    {
        return $this->repository->findBy($criteria, $order);
    }
}