<?php
namespace Api\Service;

use Api\Entity\Player;
use Doctrine\Common\Persistence\ManagerRegistry as Doctrine;

class PlayerService
{
    protected $doctrine;

    protected $em;

    protected $repository;

    public function __construct(Doctrine $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->em = $doctrine->getManager();
        $this->repository = $this->doctrine->getRepository('Api\\Entity\\Player');
    }

    public function persist(Player $player)
    {
        $this->em->persist($player);
        $this->em->flush($player);
    }
}