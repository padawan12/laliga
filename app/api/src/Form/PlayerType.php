<?php
namespace Api\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Api\Entity\Player;

class PlayerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('dorsal', IntegerType::class)
            ->add('position', TextType::class)
            ->add('nationality', TextType::class)
            ->add('club', EntityType::class, [
                'class' => Player::class,
                'choice_label' => 'name'
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Api\Entity\Player',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'player';
    }
}