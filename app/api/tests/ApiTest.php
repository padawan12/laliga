<?php
namespace Api\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiTest extends WebTestCase
{
    public function testApiStatus()
    {
        $client = static::createClient();

        $client->request('GET', '/clubs');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', '/clubs/1');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', '/clubs/1/players');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', '/clubs/1/players/1');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testApiStructure()
    {
        $client = static::createClient();

        $client->request('GET', '/clubs/1');
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('city', $data);
        $this->assertArrayHasKey('stadium', $data);
        $this->assertArrayHasKey('division', $data);
        $this->assertArrayHasKey('coach', $data);

        $client->request('GET', '/clubs/1/players/1');
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('dorsal', $data);
        $this->assertArrayHasKey('position', $data);
        $this->assertArrayHasKey('nationality', $data);
    }
}