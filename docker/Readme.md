# Descripción

A continuación se encuentran los pasos para construir la imagen docker y echar a andar el proyecto

# Compilación de la imagen

`docker-compose build`

# Iniciar docker
`docker-composer up -d`

# Hacer que el dominio apunte a nuestro localhost

`127.0.0.1 api.laliga.docker laliga.docker`

